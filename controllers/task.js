const Task = require('../models/Task')

module.exports.createTask = (clientInput) =>{
	let taskName = clientInput.name;

	let newTask = new Task({
		name: taskName

	})
return newTask.save().then((task,error)=>{
	if(error){
      return 'Saving new task failed';
   }else{
      return 'A task has created successfully';
   }
 })
}

module.exports.getAllTask =()=>{

	return Task.find({}).then(searchResult =>{
	return searchResult;

	return Task.find({}).then(searchResult =>{
	return searchResult;

	})
 })
}

module.exports.getProfileTask =(data)=>{
	return Task.findById(data).then(result =>{
	return result; 

	})

}

module.exports.deleteTask =(userId)=> {
	return Task.findByIdAndRemove(userId).then((removedTask,err)=>{
		if (removedTask) {
			return `${removedTask} Account Deleted Successfully`;
		} else {
			return 'No accounts were removed!';
		}

	})
}

module.exports.updateTask = (userId, newContent) => {
let  taskStatus = newContent.status


return Task.findById(userId).then((foundTask,error) =>{

	if (foundTask) {
		foundTask.status = taskStatus;
		return foundTask.save().then((updatedTask,saveErr)=>{
			if (saveErr) {
				return false;
			} else {
				return updatedTask;
			}
		})
	} else {
		return 'Status is sill in default'
	}
})
}