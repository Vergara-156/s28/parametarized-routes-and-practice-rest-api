const User = require('../models/Users')
const bcrypt = require('bcrypt')
module.exports.registerUser = (reqBody) => {
	let fName = reqBody.firstName		
	let lName = reqBody.lastName	
	let email = reqBody.email	
	let passW = reqBody.password


	let user = new User({
		firstName: fName,
		lastName: lName,
		email: email,
		password : bcrypt.hashSync(passW,10)
	});

	return user.save().then((user,error) =>{
		if (user){
			return user
		} else {
			return 'Failed to Create User'
		}
})

}
module.exports.getAllUsers = ()=>{
	return User.find({}).then(resultOfQuery =>{
		return resultOfQuery
	});

}
module.exports.getProfile =(data)=>{
	return User.findById(data).then(result =>{
	//	result.password = ''; 
		return result; 

	})


}

module.exports.deleteUser =(userId)=> {
	return User.findByIdAndRemove(userId).then((removedUser,err)=>{
		if (removedUser) {
			return `${removedUser}Account Deleted Successfully`
		} else {
			return 'No accounts were removed!'
	;
		}

	})
}

module.exports.updateUser = (userId, newContent) => {
let  fName = newContent.firstName
let  lName = newContent.lastName

return User.findById(userId).then((foundUser,error) =>{

	if (foundUser) {
		foundUser.firstName = fName;
		foundUser.lastName = lName
		return foundUser.save().then((updatedUser,saveErr)=>{
			if (saveErr) {
				return false;
			} else {
				return updatedUser;
			}
		})
	} else {
		return 'No User Found'
	}
})
}