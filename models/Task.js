const mongoose = require('mongoose')
const taskBlueprint = new mongoose.Schema({

	name:{
		type: String,
		required: [true,'Task name is required']
	},

	status:{
		type: String,
		default:'pending'
	}


})

module.exports= mongoose.model("Task",taskBlueprint);

