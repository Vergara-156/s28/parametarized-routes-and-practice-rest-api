const mongoose = require('mongoose')

const userBlueprint = new mongoose.Schema({

	firstName:{
		type: String,
		required: [true,'First Name is required']
	},

	lastName:{
		type: String,
		required: [true,'Last Name is required']
	},

	email:{
		type: String,
		required: [true,'email address is required']
	},

	password:{
		type: String,
		required: [true,'password is required']
	},

	isAdmin:{
		type: Boolean,
		default: false
	},

	task: [
		{
			taskId:{
				type: String,
				required: [true, 'Task ID is required']
			},

			assignedOn:{
				type: Date, 
				default: new Date()
			}
		}
	]



})

module.exports = mongoose.model("User", userBlueprint);