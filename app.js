const express = require('express');
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoutes = require('./routes/task')
const userRoutes = require('./routes/users')

dotenv.config()

let secret = process.env.CONNECTION_STRING
const server = express();
const port = process.env.PORT

server.use(express.json())
server.use('/task', taskRoutes)
server.use('/users', userRoutes)

mongoose.connect(secret);

let db = mongoose.connection;
db.once('open', ()=> console.log('Now Connect to MongoDB Atlas'));

server.listen(port, ()=> console.log(`Server is running at port ${port}`))
server.get('/',(req,res) =>{
	res.send('Welcome to App')

})