const express = require('express')
const controller = require('../controllers/task')
const route = express.Router()

route.post('/create',(req,res)=>{
	let taskInfo = req.body

	controller.createTask(taskInfo).then(result=>
		res.send(result))	

})

route.get('/',(req,res) =>{
	controller.getAllTask().then(result => {
		res.send(result);
	})

})
route.get('/:details', (req,res)=>{
let userId = req.params.details
	controller.getProfileTask(userId).then(outcome=>{
		res.send(outcome);
	})
})	
route.delete('/:id', (req,res)=>{
	let userId = req.params.id
	controller.deleteTask(userId).then(outcome=>{
		res.send(outcome)
	})

});

route.put('/:id', (req,res)=>{
	let id = req.params.id 
	let body = req.body
	//console.log(id)
	let taskStatus = req.body.status
	//console.log(fName)
	//let lName= req.body.lastName
	//console.log(lName)
	controller.updateTask(id,body).then(outcome=>{
		res.send(outcome)
	})
})
module.exports = route;


