const exp = require('express')
const controller = require('../controllers/users')
const route = exp.Router()

route.post('/register',(req,res)=>{
	let data =req.body;
	controller.registerUser(data).then(result=>
		res.send(result));


})

route.get('/',(req,res) =>{
	controller.getAllUsers().then(result => {
		res.send(result);
	})

})

route.get('/:details', (req,res)=>{
	let userId = req.params.details
	controller.getProfile(userId).then(outcome=>{
		res.send(outcome);
	})
})

route.delete('/:id', (req,res)=>{
	let userId = req.params.id
	controller.deleteUser(userId).then(outcome=>{
		res.send(outcome)
	})

});

route.put('/:id', (req,res)=>{
	let id = req.params.id 
	let body = req.body
	//console.log(id)
	//let fName = req.body.firstName
	//console.log(fName)
	//let lName= req.body.lastName
	//console.log(lName)
	controller.updateUser(id,body).then(outcome=>{
		res.send(outcome)
	})
})
module.exports =route;